﻿using EmployeeTracker.DTO.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeTracker.DTO.ViewModel
{
    public class AttendanceViewModelDTO
    {
        public int AttendanceID { get; set; }
        public int EmployeeID { get; set; }
        public int AttendanceTypeID { get; set; }
        public DateTime OffDate { get; set; }
  
        public  AttendanceTypeDTO AttendanceType { get; set; }
    }
}
