﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeTracker.DTO.Client
{
    public class EmployeeDTO
    {
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime HireDate { get; set; }

        public  ICollection<AttendanceDTO> Attendances { get; set; }
    }
}
