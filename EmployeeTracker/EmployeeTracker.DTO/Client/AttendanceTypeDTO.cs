﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EmployeeTracker.DTO.Client
{
    public class AttendanceTypeDTO
    {
        public int AttendanceTypeID { get; set; }

        public string Description { get; set; }
    }
}
