﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EmployeeTracker.DTO.Client
{
    public class AttendanceDTO
    {

        public int AttendanceID { get; set; }
        public int EmployeeID { get; set; }

        public int AttendanceTypeID { get; set; }
        [DataType(DataType.Date)]
        public DateTime OffDate { get; set; }

        public  EmployeeDTO Employee { get; set; }
        [UIHint("AttendanceTypeEditor")]
        public  AttendanceTypeDTO AttendanceType { get; set; }
    }
}
