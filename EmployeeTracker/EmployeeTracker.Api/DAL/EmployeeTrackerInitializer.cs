﻿using EmployeeTracker.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeTracker.Api.DAL
{
    public class EmployeeTrackerInitializer : System.Data.Entity.CreateDatabaseIfNotExists<EmployeeTrackerContext>
    {
        protected override void Seed(EmployeeTrackerContext context)
        {
            var employees = new List<Employee>();

            for (int i = 1; i <= 40; i++)
            {
                var emp = new Employee
                {
                    EmployeeID = i,
                    FirstName = "John-" + i,
                    LastName = "Doe-" + i,
                    HireDate = DateTime.Parse("2018-01-01").AddDays(i)
                };
                employees.Add(emp);
            }

            employees.ForEach(s => context.Employees.Add(s));
            context.SaveChanges();


            var attendanceTypes = new List<AttendanceType> {
                        new AttendanceType{AttendanceTypeID=1,Description="Sick day"},
                        new AttendanceType{AttendanceTypeID=2,Description="Personal day"},
                        new AttendanceType{AttendanceTypeID=3,Description="Vacation day"},
                        new AttendanceType{AttendanceTypeID=4,Description="Unpaid day"},
                        new AttendanceType{AttendanceTypeID=5,Description="Remote day"},
            };

            attendanceTypes.ForEach(s => context.AttendanceTypes.Add(s));
            context.SaveChanges();


            var attendance = new List<Attendance> {
                        new Attendance{ EmployeeID=1, AttendanceTypeID=1,OffDate=DateTime.Now.AddDays(-1)},
                        new Attendance{ EmployeeID=1, AttendanceTypeID=2,OffDate=DateTime.Now.AddDays(-5)},
                        new Attendance{ EmployeeID=1, AttendanceTypeID=3,OffDate=DateTime.Now.AddDays(-10)},
                        new Attendance{ EmployeeID=1, AttendanceTypeID=4,OffDate=DateTime.Now.AddDays(-15)},

                        new Attendance{ EmployeeID=3, AttendanceTypeID=1,OffDate=DateTime.Now.AddDays(-1)},
                        new Attendance{ EmployeeID=3, AttendanceTypeID=2,OffDate=DateTime.Now.AddDays(-5)},
                        new Attendance{ EmployeeID=3, AttendanceTypeID=3,OffDate=DateTime.Now.AddDays(-10)},
                        new Attendance{ EmployeeID=3, AttendanceTypeID=4,OffDate=DateTime.Now.AddDays(-15)},

                        new Attendance{ EmployeeID=5, AttendanceTypeID=1,OffDate=DateTime.Now.AddDays(-1)},
                        new Attendance{ EmployeeID=5, AttendanceTypeID=2,OffDate=DateTime.Now.AddDays(-5)},
                        new Attendance{ EmployeeID=5, AttendanceTypeID=3,OffDate=DateTime.Now.AddDays(-10)},
                        new Attendance{ EmployeeID=5, AttendanceTypeID=4,OffDate=DateTime.Now.AddDays(-15)},

                        new Attendance{ EmployeeID=10, AttendanceTypeID=1,OffDate=DateTime.Now.AddDays(-1)},
                        new Attendance{ EmployeeID=10, AttendanceTypeID=2,OffDate=DateTime.Now.AddDays(-5)},
                        new Attendance{ EmployeeID=10, AttendanceTypeID=3,OffDate=DateTime.Now.AddDays(-10)},
                        new Attendance{ EmployeeID=10, AttendanceTypeID=4,OffDate=DateTime.Now.AddDays(-15)},

                        new Attendance{ EmployeeID=15, AttendanceTypeID=1,OffDate=DateTime.Now.AddDays(-1)},
                        new Attendance{ EmployeeID=15, AttendanceTypeID=2,OffDate=DateTime.Now.AddDays(-5)},
                        new Attendance{ EmployeeID=15, AttendanceTypeID=3,OffDate=DateTime.Now.AddDays(-10)},
                        new Attendance{ EmployeeID=15, AttendanceTypeID=4,OffDate=DateTime.Now.AddDays(-15)},

                        new Attendance{ EmployeeID=20, AttendanceTypeID=1,OffDate=DateTime.Now.AddDays(-1)},
                        new Attendance{ EmployeeID=20, AttendanceTypeID=3,OffDate=DateTime.Now.AddDays(-5)},
                        new Attendance{ EmployeeID=20, AttendanceTypeID=4,OffDate=DateTime.Now.AddDays(-10)},
                        new Attendance{ EmployeeID=20, AttendanceTypeID=5,OffDate=DateTime.Now.AddDays(-15)},

                        new Attendance{ EmployeeID=25, AttendanceTypeID=1,OffDate=DateTime.Now.AddDays(-1)},
                        new Attendance{ EmployeeID=25, AttendanceTypeID=3,OffDate=DateTime.Now.AddDays(-5)},
                        new Attendance{ EmployeeID=25, AttendanceTypeID=4,OffDate=DateTime.Now.AddDays(-10)},
                        new Attendance{ EmployeeID=25, AttendanceTypeID=5,OffDate=DateTime.Now.AddDays(-15)},

                        new Attendance{ EmployeeID=30, AttendanceTypeID=1,OffDate=DateTime.Now.AddDays(-1)},
                        new Attendance{ EmployeeID=30, AttendanceTypeID=3,OffDate=DateTime.Now.AddDays(-5)},
                        new Attendance{ EmployeeID=30, AttendanceTypeID=4,OffDate=DateTime.Now.AddDays(-10)},
                        new Attendance{ EmployeeID=30, AttendanceTypeID=5,OffDate=DateTime.Now.AddDays(-15)}

            };

            attendance.ForEach(s => context.Attendance.Add(s));
            context.SaveChanges();

        }
    }
}