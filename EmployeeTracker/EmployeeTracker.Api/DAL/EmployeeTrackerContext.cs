﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using EmployeeTracker.Api.Models;

namespace EmployeeTracker.Api.DAL
{
    public class EmployeeTrackerContext : DbContext
    {
        public EmployeeTrackerContext() : base("EmployeeTrackerContext")
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Attendance> Attendance { get; set; }
        public DbSet<AttendanceType> AttendanceTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}