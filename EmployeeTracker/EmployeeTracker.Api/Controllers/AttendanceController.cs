﻿using EmployeeTracker.Api.DAL;
using EmployeeTracker.Api.Models;
using EmployeeTracker.DTO.Client;
using EmployeeTracker.DTO.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace EmployeeTracker.Api.Controllers
{
    public class AttendanceController : ApiController
    {
        private static bool UpdateDatabase = false;

        private EmployeeTrackerContext db = new EmployeeTrackerContext();

        // POST: api/Employees
        [Route("api/AddAttendance")]
        [HttpPost]
        [ResponseType(typeof(AttendanceDTO))]
        public IHttpActionResult PostAttendance(string jsonAttendance)
        {

            if (!string.IsNullOrEmpty(jsonAttendance))
            {
                return BadRequest(ModelState);
            }

            var attendance = JsonConvert.DeserializeObject<AttendanceDTO>(jsonAttendance);

            var model = new Attendance()
            {
                AttendanceTypeID = attendance.AttendanceTypeID,
                EmployeeID = attendance.EmployeeID,
                OffDate = attendance.OffDate
            };

            db.Attendance.Add(model);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new
            {
                id = model.EmployeeID
            }, attendance);
        }


        public IList<AttendanceViewModelDTO> GetAll()
        {
            var result = HttpContext.Current.Session["Attendance"] as IList<AttendanceViewModelDTO>;

            if (result == null || UpdateDatabase)
            {
                result = db.Attendance.Select(a => new AttendanceViewModelDTO
                {
                    AttendanceID = a.AttendanceID,
                    AttendanceType = new AttendanceTypeDTO()
                    {
                        AttendanceTypeID = a.AttendanceType.AttendanceTypeID,
                        Description = a.AttendanceType.Description
                    },
                    EmployeeID = a.EmployeeID,
                    AttendanceTypeID = a.AttendanceTypeID,
                    OffDate = a.OffDate
                }).ToList();

                HttpContext.Current.Session["Attendance"] = result;
            }

            return result;
        }

        [Route("api/GetAttendanceViewModel")]
        public IEnumerable<AttendanceViewModelDTO> Read()
        {
            return GetAll();
        }


        [Route("api/GetAttendanceByEmployeeId")]
        [HttpGet]
        public IHttpActionResult GetAttendanceByEmployeeId(Int32 id)
        {
            var result = (from emp in db.Attendance
                          where emp.EmployeeID == id
                          select new AttendanceDTO()
                          {
                              AttendanceID = emp.AttendanceID,
                              AttendanceType = new AttendanceTypeDTO()
                              {
                                  AttendanceTypeID = emp.AttendanceType.AttendanceTypeID,
                                  Description = emp.AttendanceType.Description
                              },
                              EmployeeID = emp.EmployeeID,
                              AttendanceTypeID = emp.AttendanceTypeID,
                              OffDate = emp.OffDate
                          }).ToList();

            return Json(result);
        }
    }
}
