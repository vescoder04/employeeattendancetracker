﻿using EmployeeTracker.Api.DAL;
using EmployeeTracker.DTO.Client;

using System.Linq;

using System.Web.Http;

namespace EmployeeTracker.Api.Controllers
{
    public class AttendanceTypeController : ApiController
    {
        private EmployeeTrackerContext db = new EmployeeTrackerContext();

        [Route("api/GetAttendanceTypes")]
        [HttpGet]
        public IHttpActionResult GetAttendanceType()
        {
            var result = (from emp in db.AttendanceTypes
                          select new AttendanceTypeDTO
                          {
                              AttendanceTypeID = emp.AttendanceTypeID,
                              Description = emp.Description
                          }).ToList();

            return Json(result);
        }



    }
}
