﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using EmployeeTracker.Api.DAL;
using EmployeeTracker.Api.Models;
using EmployeeTracker.DTO.Client;
using Newtonsoft.Json;

namespace EmployeeTracker.Api.Controllers
{
    public class EmployeesController : ApiController
    {
        private EmployeeTrackerContext db = new EmployeeTrackerContext();

        [Route("api/GetEmployees")]
        [HttpGet]
        public IHttpActionResult GetEmployees()
        {

            var result = (from emp in db.Employees
                          select new EmployeeDTO()
                          {
                              Attendances = (from p in emp.Attendances
                                             select new AttendanceDTO()
                                             {
                                                 AttendanceID = p.AttendanceID,
                                                 AttendanceTypeID = p.AttendanceTypeID,
                                                 EmployeeID = p.EmployeeID,
                                                 OffDate = p.OffDate,
                                                 AttendanceType= new AttendanceTypeDTO()
                                                 {
                                                     AttendanceTypeID=p.AttendanceType.AttendanceTypeID,
                                                     Description=p.AttendanceType.Description
                                                 }
                                             }).ToList(),
                              EmployeeID = emp.EmployeeID,
                              FirstName = emp.FirstName,
                              LastName = emp.LastName,
                              HireDate = emp.HireDate
                          }).ToList();

            return Json(result);
        }

        // GET: api/Employees/5
        [Route("api/GetEmployeeById")]
        [HttpGet]
        [ResponseType(typeof(EmployeeDTO))]
        public IHttpActionResult GetEmployee(int id)
        {
            var employee = (from emp in db.Employees
                            where emp.EmployeeID == id
                            select new EmployeeDTO()
                            {
                                Attendances = (from p in emp.Attendances
                                               select new AttendanceDTO()
                                               {
                                                   AttendanceID = p.AttendanceID,
                                                   AttendanceTypeID = p.AttendanceTypeID,
                                                   EmployeeID = p.EmployeeID,
                                                   OffDate = p.OffDate,
                                                   AttendanceType = new AttendanceTypeDTO()
                                                   {
                                                       AttendanceTypeID = p.AttendanceType.AttendanceTypeID,
                                                       Description = p.AttendanceType.Description
                                                   }
                                               }).ToList(),
                                EmployeeID = emp.EmployeeID,
                                FirstName = emp.FirstName,
                                LastName = emp.LastName,
                                HireDate = emp.HireDate
                            }).FirstOrDefault();
            if (employee == null)
            {
                return NotFound();
            }

            return Json(employee);
        }

        // PUT: api/Employees/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEmployee(int id, EmployeeDTO employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != employee.EmployeeID)
            {
                return BadRequest();
            }

            var employeeModel = new Employee()
            {
                FirstName = employee.FirstName,


            };

            db.Entry(employeeModel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Employees
        [ResponseType(typeof(Employee))]
        public IHttpActionResult PostEmployee(Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Employees.Add(employee);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = employee.EmployeeID }, employee);
        }

        // DELETE: api/Employees/5
        [ResponseType(typeof(Employee))]
        public IHttpActionResult DeleteEmployee(int id)
        {
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return NotFound();
            }

            db.Employees.Remove(employee);
            db.SaveChanges();

            return Ok(employee);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeExists(int id)
        {
            return db.Employees.Count(e => e.EmployeeID == id) > 0;
        }
    }
}