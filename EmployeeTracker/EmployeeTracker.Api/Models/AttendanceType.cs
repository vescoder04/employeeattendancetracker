﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EmployeeTracker.Api.Models
{
    public class AttendanceType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AttendanceTypeID { get; set; }

        public string Description { get; set; }
    }
}