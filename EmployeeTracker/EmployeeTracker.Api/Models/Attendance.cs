﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EmployeeTracker.Api.Models
{

    public class Attendance
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AttendanceID { get; set; }
        public int EmployeeID { get; set; }
        public int AttendanceTypeID { get; set; }
        public DateTime OffDate { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual AttendanceType AttendanceType { get; set; }
    }
}