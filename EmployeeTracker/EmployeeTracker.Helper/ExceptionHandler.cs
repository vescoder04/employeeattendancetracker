﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace EmployeeTracker.Helper
{
    public class ExceptionHandler
    {
        private static ExceptionHandler _instance;
        public static ExceptionHandler Instance
        {
            get
            {
                if (_instance == null) _instance = new ExceptionHandler();
                return _instance;
            }
        }

        public void HandleException(StackFrame sf, Exception e)
        {
            string message = "File: " + sf.GetFileName() + Environment.NewLine;
            message += "Date and Time: " + DateTime.Now + Environment.NewLine;
            message += "Method: " + sf.GetMethod().Name + Environment.NewLine;
            message += "Line Number: " + sf.GetFileLineNumber() + Environment.NewLine;
            message += "Column Number: " + sf.GetFileColumnNumber() + Environment.NewLine;
            if (e.InnerException != null)
                message += "Inner Exception Message: " + e.InnerException.Message + Environment.NewLine;
            message += Environment.NewLine + "==========================" + Environment.NewLine;
            message += e.Message + Environment.NewLine;
            message += e.StackTrace;
            //Email.sendEmailException(message);
           //Log.Write(message);
        }

        public void HandleException(StackFrame sf, Exception e, String customMesage = "")
        {
            string message = "File: " + sf.GetFileName() + Environment.NewLine;
            message += "Date and Time: " + DateTime.Now + Environment.NewLine;
            message += "Method: " + sf.GetMethod().Name + Environment.NewLine;
            message += "Line Number: " + sf.GetFileLineNumber() + Environment.NewLine;
            message += "Column Number: " + sf.GetFileColumnNumber() + Environment.NewLine;
            message += "Custom Message: " + customMesage + Environment.NewLine;
            if (e.InnerException != null)
                message += "Inner Exception Message: " + e.InnerException.Message + Environment.NewLine;
            message += "==========================" + Environment.NewLine;
            message += e.Message + Environment.NewLine;
            message += e.StackTrace;
            //Email.sendEmailException(message);
            //Log.Write(message);
        }
    }
}
