﻿using EmployeeTracker.DTO.Client;
using EmployeeTracker.DTO.ViewModel;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EmployeeTracker.Web.Controllers
{
    public class AttendanceController : BaseController
    {
        // GET: Attendance
        public ActionResult Index()
        {
            populateAttendanceTypes();
            return View();
        }


        public async Task<ActionResult> Employee_Details([DataSourceRequest]DataSourceRequest request)
        {
            var server_response = await GetAsync<List<EmployeeDTO>>("GetEmployees");
            return Json(server_response.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        public async Task<ActionResult> Employee_Details_Attendance(int employeeID, [DataSourceRequest] DataSourceRequest request)
        {
            var server_response = await GetAsync<List<AttendanceDTO>>("GetAttendanceByEmployeeId?id=" + employeeID);

            return Json(server_response.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }




        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Popup_Create([DataSourceRequest] DataSourceRequest request, AttendanceDTO attendance)
        {
            var server_response = new AttendanceDTO();
            if (attendance != null && ModelState.IsValid)
            {
                server_response = await Post<AttendanceDTO, AttendanceDTO>("AddAttendance", attendance);
            }

            return Json(new[] { server_response }.ToDataSourceResult(request, ModelState));
        }



        [HttpGet]
        public async Task<ActionResult> Popup_Read([DataSourceRequest] DataSourceRequest request)
        {
            var server_response = await GetAsync<List<AttendanceViewModelDTO>>("GetAttendanceViewModel");

            return Json(server_response.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }




        #region "functions"
        /// <summary>
        /// Populate the dropdown for Attendance Types
        /// </summary>
        public void populateAttendanceTypes()
        {
            var result = Task.Run(()=>GetAsync<List<AttendanceTypeDTO>>("GetAttendanceTypes"));

            ViewData["attendanceTypes"] = result.Result.OrderBy(e => e.Description);
            ViewData["defaultAttendanceTypes"] = result.Result.First();
        }

        #endregion

    }


}